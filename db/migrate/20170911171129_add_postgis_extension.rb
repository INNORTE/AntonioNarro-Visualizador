class AddPostgisExtension < ActiveRecord::Migration[5.1]
  def up
    execute %{
      create extension postgis;
    }
  end

  def down
    execute %{
      drop extension postgis;
    }
  end
end
