class CreateTranslates < ActiveRecord::Migration[5.1]
  def change
    create_table :translates do |t|
      t.string :layer
      t.string :table

      t.timestamps
    end
  end
end
