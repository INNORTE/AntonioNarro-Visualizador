# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Translate.create(layer:'Área Natural Protegida',table:'public.anp',campos:'name,descriptio')
Translate.create(layer:'Estaciones Climatológicas',table:'public.estaciones_climatologicas',campos:'name,description,condicion')
Translate.create(layer:'Degradación de Suelo 250k',table:'public.degra250k',campos:'tipo,grado,causa,area')
Translate.create(layer:'Degradación de Suelo 250k (c)',table:'public.degra250kc',campos:'tipo,grado,causa,area')
Translate.create(layer:'Corrientes de Agua',table:'public.corrientes_agua',campos:'entidad,tipo,fc,shape_len')
Translate.create(layer:'Núcleos Agrarios',table:'public.nucleos_agrarios',campos:'nom_nuc,municipio,clv_unica,fol_matriz,tipo,edo_clv,mun_clv,programa')
# Translate.create(layer:'Estaciones Climatológicas',table:'public.estaciones_climatologicas',campos:'name,description,condicion')
