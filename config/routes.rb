Rails.application.routes.draw do
  root 'mapa#index'
  get '/ajax',to: 'mapa#ajax'
  get '/gfi', to: 'mapa#getFeatureInfo'
end
