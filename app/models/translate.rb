class Translate < ApplicationRecord
  def executeSQL(table,columns,coords)
    results = ActiveRecord::Base.connection.exec_query("select #{columns} from #{table} where st_intersects(geom,st_buffer(st_setsrid(st_point(#{coords[0]},#{coords[1]}),4326),0.05)) = true")
    return results.to_hash if results.present?
  end

end
