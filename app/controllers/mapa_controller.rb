class MapaController < ApplicationController
  layout 'mapa'
  before_action :getParams, except:[:index]
  def index
  end

  def ajax

  end

  def getFeatureInfo
    results = []
    @layers.each do |l|
      res = []
      t = Translate.where("layer = ?",l)
      t.map do |tab|
        res << Translate.new.executeSQL("#{tab.table}",tab.campos,@coords)
      end
      results << {l => res.flatten}
    end
    # Response to client with data
    respond_to do |format|
      format.html {render html: '<strong>Nada que ver por aquí XD</strong>'.html_safe}
      format.json {render json: results}
      format.xml {render xml: results}
    end
  end

  protected
  def getParams
    # implement a for with layers requested
    @layers = params[:capas].split(',')
    @coords = params[:coordenadas].split(',')
  end

end
