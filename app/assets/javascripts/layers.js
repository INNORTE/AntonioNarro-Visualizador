var layers = {
  'Núcleos Agrarios': {'LAYERS': 'Núcleos Agrarios'},
  'Degradación de Suelo 250k': {'LAYERS': 'Degradación de Suelo 250k'},
  'Degradación de Suelo 250k (c)': {'LAYERS': 'Degradación de Suelo 250k (c)'},
  'Área Natural Protegida': {'LAYERS': 'Área Natural Protegida'},
  'Estaciones Climatológicas': {'LAYERS': 'Estaciones Climatológicas'},
  'Corrientes de Agua': {'LAYERS': 'Corrientes de Agua'},
  'IPASSA': {'LAYERS': 'ipassa_pol,ipassa_l,ipassa_p'}
};
var layersExternas = {
  'Red Hidrográfica (CONABIO)': {'parametros':{'LAYERS':'hidro4mgw'},url:'http://www.conabio.gob.mx/informacion/explorer/wms?','servidor':'geoserver'}
};
