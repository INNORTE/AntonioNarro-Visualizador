//= require ol.js
var Mapa = {
  Objeto:{
    init: function(divId,center,zoom,layer){
      this.mapa = new ol.Map({
        layers: layer,
        target: divId,
        view: new ol.View({
          center: ol.proj.fromLonLat(center),
          zoom: 4,
        })
      });
      this.mapa.on('singleclick', Mapa.Objeto.obtenerLocalizacion);
    },
    mapa:{},
    capasBase: {},
    agregarTileLayer: function(nombre,source){
      this.mapa.addLayer(new ol.layer.Tile({source:source}));
    },
    obtenerLocalizacion: function(e){
      var coordenadas = Mapa.Objeto.transformTo4326(e.coordinate),capas=[],i=0;
      for(layer in Mapa.Layers.capas){
        if(Mapa.Layers.capas[layer].getVisible()){
          capas[i]=layer;
          i++;
        }
      }
      capas.length==0 ? Mapa.FeaturesInfo.noCapas() : Mapa.Ajax.peticion(window.location.href + "/gfi.json?capas="+capas+"&coordenadas="+coordenadas);
    },
    transformTo4326: function(coordenadas){
      return ol.proj.transform(coordenadas,'EPSG:3857','EPSG:4326');
    }
  },
  Layers:{
    init:function(panel,capas){
      this.arbol = panel;
      this.capas = capas;
      for(k in this.capas){
        this.anadir(k,this.capas[k]);
        this.anadirArbolCapas(k,Mapa.Layers.toggleCapa);
      }
    },
    anadir:function(nombre,opciones){
      var capa = new ol.layer.Tile({
        source: new ol.source.TileWMS({
          url: this.url,
          params: opciones,
          serverType: 'mapserver'
        }),
        visible: false
      });
      this.capas[nombre] = capa;
      this.capas[nombre].set('layers',opciones['LAYERS'])
      Mapa.Objeto.mapa.addLayer(this.capas[nombre]);
    },
    capas:{},
    arbol:{},
    url: '/nwms?',
    anadirArbolCapas:function(nombre,funcion){
      var nuevaEntrada = document.createElement('li'), enlace = document.createElement('a');
      enlace.innerHTML=nombre;
      enlace.id=nombre;
      nuevaEntrada.append(enlace);
      nuevaEntrada.addEventListener('click',funcion);
      this.arbol.append(nuevaEntrada);
    },
    toggleCapa:function(e){
      var id = e.target.getAttribute('id');
      for(layer in Mapa.Layers.capas){
        if(id == layer){
          if(Mapa.Layers.capas[layer].getVisible()){
            Mapa.Layers.apagarCapa(Mapa.Layers.capas[layer]);
            Mapa.Simbologia.eliminarSimbologia(layer);
            if(e.target.parentNode.classList.contains('active'))
              e.target.parentNode.classList.remove('active');
          }else{
            Mapa.Layers.prenderCapa(Mapa.Layers.capas[layer]);
            Mapa.Simbologia.anadirSimbologia(layer,Mapa.Layers.capas[layer].get('layers'),Mapa.Layers.url + 'mode=legend&layer=');
            if(!e.target.parentNode.classList.contains('active'))
              e.target.parentNode.classList.add('active');
          }
        }
      }
    },
    prenderCapa:function(e){
      e.setVisible(true);
    },
    apagarCapa:function(e){
      e.setVisible(false);
    },
  },
  LayersExternas:{
    init:function(arbol,capasExtra){
      this.capasExtra = capasExtra;
      for(k in this.capasExtra){
        Mapa.LayersExternas.anadir(k,this.capasExtra[k]);
        Mapa.Layers.anadirArbolCapas(k,Mapa.LayersExternas.toggleCapaExterna);
      }
    },
    capasExtra:{},
    anadir:function(nombre,opciones){
      var capaExtra = new ol.layer.Tile({
        source: new ol.source.TileWMS({
          url: opciones['url'],
          params: opciones['parametros'],
          serverType: opciones['servidor']
        }),
        visible:false
      });
      this.capasExtra[nombre] = capaExtra;
      this.capasExtra[nombre].set('layer',opciones['parametros']['LAYERS']);
      Mapa.Objeto.mapa.addLayer(this.capasExtra[nombre]);
    },
    toggleCapaExterna: function(e){
      var id = e.target.getAttribute('id');
      for(layer in Mapa.LayersExternas.capasExtra){
        if(id == layer){
          if(Mapa.LayersExternas.capasExtra[layer].getVisible()){
             Mapa.Layers.apagarCapa(Mapa.LayersExternas.capasExtra[layer]);
             Mapa.Simbologia.eliminarSimbologia(Mapa.LayersExternas.capasExtra[layer].get('layer'));
             if(e.target.parentNode.classList.contains('active'))
              e.target.parentNode.classList.remove('active')
          }else{
            Mapa.Layers.prenderCapa(Mapa.LayersExternas.capasExtra[layer]);
            Mapa.Simbologia.anadirSimbologia(Mapa.LayersExternas.capasExtra[layer].get('layer'),layer,'http://www.conabio.gob.mx/informacion/explorer/wms?REQUEST=GetLegendGraphic&VERSION=1.3.0&FORMAT=image/png&WIDTH=40&HEIGHT=40&layer=');
            if(!e.target.parentNode.classList.contains('active'))
              e.target.parentNode.classList.add('active')
          }
        }
      }
    }
  },
  Simbologia:{
    init: function(pestana,simbologia,simbContenedor,contenedor){
      this.pestana = pestana;
      this.simbologia = simbologia;
      this.simbContenedor = simbContenedor;
      this.contenedor = contenedor;
      this.pestana.onclick = Mapa.Simbologia.toggleSimbologia;
    },
    pestana:{},
    simbologia:{},
    simbContenedor:{},
    contenedor:{},
    toggleSimbologia: function(){
      Mapa.Simbologia.simbologia.classList.contains('hide-simbologia') ? Mapa.Simbologia.showSimbologia(Mapa.Simbologia.simbologia,Mapa.Simbologia.simbContenedor) : Mapa.Simbologia.hideSimbologia(Mapa.Simbologia.simbologia,Mapa.Simbologia.simbContenedor);
    },
    showSimbologia: function(e,f){
      e.classList.add('right-in');
      f.classList.remove('hidden');
      setTimeout(function(){
        e.classList.remove('hide-simbologia');
        e.classList.remove('right-in');
      },500);
    },
    hideSimbologia: function(e,f){
      e.classList.add('right-out');
      setTimeout(function(){
        e.classList.add('hide-simbologia');
        f.classList.add('hidden');
        e.classList.remove('right-out');
      },500)
    },
    anadirSimbologia: function(e,n, src){
      var nuevaSimbologia = document.createElement('li'),header = document.createElement('div'), body = document.createElement('div'), image = '', lys = n.split(',');
      nuevaSimbologia.id = e;
      header.classList.add('collapsible-header');
      body.classList.add('collapsible-body');
      header.id = e;
      header.innerHTML = e;
      Object.keys(lys).forEach(function(ly){
        image = document.createElement('img');
        image.src = src+lys[ly];
        body.append(image);
      });
      nuevaSimbologia.append(header);
      nuevaSimbologia.append(body);
      Mapa.Simbologia.contenedor.append(nuevaSimbologia);
    },
    eliminarSimbologia: function(e){
      var posicion = 0;
      Object.keys(Mapa.Simbologia.contenedor.children).forEach(function(k){
        if(Mapa.Simbologia.contenedor.children[k].getAttribute('id') == e){
          posicion=k
        }
      });
      Mapa.Simbologia.contenedor.children[posicion].remove();
    }
  },
  Ajax:{
    peticion:function(url){
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
          Mapa.FeaturesInfo.mostrar(JSON.parse(this.responseText));
        }
      };
      ajax.open("GET",url,true);
      ajax.send();
    },
    respuesta:{},
  },
  FeaturesInfo:{
    panel:{},
    mostrar:function(respuesta){
      var panel = document.createElement('div'),tabs = document.createElement('ul'),i=1;
      Mapa.FeaturesInfo.panel.innerHTML = '';
      panel.classList.add('col');
      panel.classList.add('s12');
      tabs.classList.add('tabs');
      for(capa in respuesta){
        this.addingTab(tabs,respuesta[capa],i);
        i++;
      }
      panel.append(tabs);
      Mapa.FeaturesInfo.panel.append(panel);
      i=1;
      for(capa in respuesta){
        this.addingContentTab(Mapa.FeaturesInfo.panel,respuesta[capa],i);
        i++;
      }
      $('#modal1').modal('open');
      $('ul.tabs').tabs();
    },
    addingTab:function(tabpanel,valor,indice){
      var nodo = document.createElement('li'),enlace=document.createElement('a');
      nodo.classList.add('tab');
      enlace.href = '#tab_'+indice;
      enlace.text = Object.keys(valor);
      if(indice == 1 )
        enlace.classList.add('active');
      nodo.append(enlace);
      tabpanel.append(nodo);
    },
    addingContentTab:function(panel,valor,indice){
      var tabla = document.createElement('table'),div = document.createElement('div'),first = true;
      div.id="tab_"+indice;
      if(valor[Object.keys(valor)[0]]!=null){
        var header="<thead><tr>",body="<tbody>";
        tabla.classList.add('striped');
        Object.keys(valor).forEach(function(k){
          Object.keys(valor[k]).forEach(function(l){
            if(valor[k][l] != null){
              body += "<tr>";
              Object.keys(valor[k][l]).forEach(function(m){
                if(first){
                  header += "<th>"+m+"</th>";
                }
                body += "<td>"+valor[k][l][m]+"</td>";
              });
              first = false;
              body += "</tr>";
            }
          });
          first = true;
        });
        header += "</tr></thead>";
        body += "</tbody>"
        tabla.innerHTML = header + body;
      }else{
        tabla = document.createElement('h6');
        tabla.innerHTML +=  'No se encontraron features en ésta capa tomando en cuenta un radio de 5.5 km a partir de la coordenada especificada';
      }
        div.append(tabla);
        panel.append(div);
    },
    noCapas: function(){
      swal ("¡Parece que no hay capas consultables activas!","Prueba activando una capa desde su panel" ,"error" )
    }
  },
  Busqueda:{
    init:function(activador,activado){
      this.activador = activador;
      this.activado = activado;
      this.activador.onclick= Mapa.Busqueda.toggleBusqueda;
    },
    activador:{},
    activado:{},
    toggleBusqueda:function(e){
      Mapa.Busqueda.activado.classList.contains('hidden') ? Mapa.Busqueda.mostrarBusqueda(Mapa.Busqueda.activado) : Mapa.Busqueda.ocultarBusqueda(Mapa.Busqueda.activado);
    },
    mostrarBusqueda:function(e){
      e.classList.add('fade-in-down');
      e.classList.remove('hidden');
      setTimeout(function(){
        e.classList.remove('fade-in-down');
      },400);
    },
    ocultarBusqueda:function(e){
      e.classList.add('fade-out-up');
      setTimeout(function(){
        e.classList.add('hidden');
        e.classList.remove('fade-out-up');
        e.children[0].children[0].children[0].children[0].value='';
      },400);
    }
  }
};
